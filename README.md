# AtomTestLab Documentation


## <a name="overview"></a>Overview
The AtomTestLab repo is an R&D laboratory for Reactor atom package creation. None of this content has been pre-filtered or moderated.


Reactor is a package manager created by the [We Suck Less Community](https://www.steakunderwater.com/wesuckless/viewforum.php?f=32) for Fusion and Resolve. Reactor streamlines the installation of 3rd party content through the use of "Atom" packages that are synced automatically with a Git repository.

## <a name="install"></a>Installing AtomTestLab Repo

**Step 1.** Download a copy of the Reactor package manager installer script from the [WSL forums Reactor Release Announcement ](https://www.steakunderwater.com/wesuckless/viewtopic.php?f=32&t=1814) thread, and install it in your copy of Resolve/Fusion.

**Step 2.** Download a copy of the "[Add AtomTestLab Repo.lua](https://gitlab.com/AndrewHazelden/AtomTestLab/-/blob/master/Add%20AtomTestLab%20Repo.lua)" script and Drag/Drop the script from your desktop folder into the Fusion Console window.

When the "Add AtomTestLab Repo" script is run, a window will appear. If you click the "Add Repo" button the script will add the new repository to your copy of Reactor for Resolve/Fusion.

![Add AtomTestLab Repo Script](Docs/Images/Add-AtomTestLab-Repo.png)

**Step 3.** Open the Reactor package manager. You are now able to install atom packages from both the standard Reactor repository, and the AtomTestRepo using the Reactor window. If you look in part of the Reactor package manager window where all of the packages are listed, you will notice a "Repo" heading that indicates if an atom is sourced from either "Reactor" or the "AtomTestLab".

If you want to filter the list of atom packages by the individual repo they came from, click on the word "All" which is the first entry in the "Repo" ComboMenu at the top left of the Reactor window. With this menu, you are able to select the "AtomTestRepo" entry, or the "Reactor" entry to choose a specific repo you want to browse. Setting the ComboMenu back to "All" when you are done will show a combined atom package view which displays the content from all of the active repos at once.

![Reactor Window](Docs/Images/Atom-Test-Lab-Repo-Menu.png)

## <a name="remove"></a>Remove the AtomTestLab Repo

If you would like to remove the AtomTestLab Repo from your system, you can re-launch the `Add AtomTestLab Repo.lua` script. Clicking the "Remove Repo" button will edit the `Reactor:/System/Reactor.cfg` file to remove the AtomTestLab repository entry.

You can optionally navigate to the `Reactor:/Atoms/` PathMap folder and remove the `AtomTestLab` entry you see there. This folder holds the locally cached copy of .atom package files.

![Atoms Folder](Docs/Images/Reactor-Atoms-Folder.png)

## <a name="cfg"></a>Manually Editing the Reactor.cfg File

Reactor uses a configuration file stored at `Reactor:/System/Reactor.cfg` to manage the active repositories. The document format for this configuration file is known as a Lua table structure which is stored in a plain-text formatted document.

Reactor.cfg File Contents:


	{
		Repos = {
			_Core = {
				Protocol = "GitLab",
				ID = "31308292"
			},
			AtomTestLab = {
				Protocol = "GitLab",
				ID = "31308292"
			},
			Reactor = {
				Protocol = "GitLab",
				ID = "5058837"
			}
		},
		Settings = {
			Reactor = {
				ConcurrentTransfers = 1,
				MarkAsNew = true,
				LiveSearch = true,
				PrevSyncTime = 1636914268,
				NewForDays = 7,
				ViewLayout = "Balanced View",
				AskForInstallScriptPermissions = true
			}
		}
	}

 
Each repository that is listed in the `Reactor.cfg` file has its own entry that uses the GitLab Project ID code to link to a collection of atom packages that are placed in an `Atoms` folder at the base of a GitLab repository. If you go to the GitLab webpage for a project you will be able to spot the Project ID code just under the heading title:

<hr>

<a href="https://gitlab.com/WeSuckLess/Reactor/">GitLab Reactor</a> Project ID: 5058837  

![GitLab Project ID](Docs/Images/GitLab-Project-ID-Reactor.png)

<hr>

<a href="https://gitlab.com/AndrewHazelden/AtomTestLab">GitLab AtomTestLab</a> Project ID: 31308292  

![GitLab Project ID](Docs/Images/GitLab-Project-ID-AtomTestLab.png)

<hr>

## <a name="resync"></a>Resync the Repository

After you manually add a new repository entry to your Reactor.cfg file, the next time you launch the Reactor package manager, you should run the `Reactor > Tools > Resync Repository` menu item.

![Resync Repository](Docs/Images/Resync-Repository-Menu.png)

The resyncing process will refresh the previous commit ID tag, and re-download the atom package files for all of the active repositories.

Last Revised 2021-12-03

