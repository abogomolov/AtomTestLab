--[[--
Hypertext Compositor Editor v2 2021-12-09
by Andrew Hazelden <andrew@andrewhazelden.com>
www.andrewhazelden.com
--]]--

if comp then
	comp:RunScript("Config:/HypertextCompositor/HypertextCompositorEditor.lua" , {})
else
	print('[Comp Error] Please run this script from inside the Fusion page.')
end
